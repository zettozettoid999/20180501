<?php
function __autoload($className)
{
	if (strpos($className,'Controller') !== false)
	{
		$fileName = 'controller/'.$className.'.php';
	}
	elseif (strpos($className,'Flickr') !== false)
	{
		$fileName = '3rd_party/phpflickr-master/phpFlickr.php';
	}
	else
	{
		$fileName = 'classes/'.$className.'.php';		
	}
	require_once $fileName;
}

session_start();

if (
		   !empty($_SESSION['userName'])
		&& isset($_REQUEST['ajax'])
		&& $_REQUEST['ajax'] == 1
		&& !empty($_REQUEST['method'])
)
{
	$method = 'ajax' . ucfirst(strtolower($_REQUEST['method']));
	$controllerName = strtolower(empty($_REQUEST['controller']) ? 'home' : $_REQUEST['controller']) . 'Controller';
	$controller = new $controllerName();
	$res = call_user_func_array(array($controller, $method), array($_GET));
	echo json_encode($res);
	die;
}
?>
<!DOCTYPE html>
<html lang="hu-HU">
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="css/style.css"/> 
</head>
<html>
<body>
<?php

$stem = empty($_REQUEST['page']) ? 'home' : $_REQUEST['page'];
$className = $stem . 'Controller';
$controller = new $className('', true);

$controller->render();
?>

</body>
</html>