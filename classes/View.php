<?php

class View
{
	public $content = '';
	public $variables = array();
	protected $templateDir = '';
	
	public function __construct($view)
	{
		$this->content = file_get_contents('view/'.$view.'.php');
	}
	
	public function setTemplateDir($dir)
	{
		$this->templateDir = $dir;
	}
	
	public function assign($variable, $value)
	{
		$this->variables[$variable] = $value;
	}
	
	public function __toString()
	{
		$content = '?> '.$this->content;
                extract($this->variables, EXTR_REFS);
                foreach ($this->variables as $key => $value)
                {
                    $this->$key = $$key;
                }
		return (string) eval($content);
	}
}