<?php
if(!function_exists('get_called_class'))
{
    function get_called_class()
    {
        $t = debug_backtrace();
		$t = $t[1];
        if ( isset( $t['object'] ) && $t['object'] instanceof $t['class'] )
            return get_class( $t['object'] );
        return false;
    }
}

class baseController
{
    public $stem;
    public $model;
    public $view;
    public $params;
    public $display;
    
    public function __construct($params = null, $display = true)
    {
        $calledClass = get_called_class();
        $this->stem = (str_replace('Controller', '', $calledClass));
        $this->model = (str_replace('Controller', 'Model', $calledClass));
        $this->params = $params;
        $this->display = $display;
    }
    
    public function render()
    {
		empty($this->params['view']) || $this->view = $this->params['view'];
        $view = new View(empty($this->view) ? $this->stem : $this->view);

        if (!empty($this->params))
        {
            foreach ($this->params as $key => $value)
            {
                $view->assign($key, $value);
            }
        }

        if ($this->display)
        {
			echo $view;
			return;
        }

		return $view;
    }
}