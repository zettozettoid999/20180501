<?php
//require_once '3rd_party/phpflickr-master/phpFlickr.php';

class homeController extends baseController
{
    public function __construct()
    {
        return parent::__construct();
    }
    
    public function render()
    {
        $this->params['searchTags'] = $this->parseSearchTagsConf();
        
        $this->params['res'] = array();
        $this->params['tags'] = 'summer,cars';
        if (!empty($_REQUEST['tags']))
        {
            $this->params['tags'] = $_REQUEST['tags'];
        }
        elseif (!empty($_REQUEST['tags_dropdown']))
        {
            $this->params['tags'] = $_REQUEST['tags_dropdown'];
        }
        $this->params['tags'] = str_replace(' ', ',', $this->params['tags']);
        
        $f = new phpFlickr('57f694132e4714c29a64c9af890b124e');
        $perPage = 20;
        $res = $f->photos_search(array("tags"=>$this->params['tags'], "tag_mode"=>"any", 'per_page'=>$perPage));
        if (!empty($res['photo']))
        {
            foreach($res['photo'] as $key => $photo)
            {
                $res['photo'][$key]['url']   = $f->buildPhotoURL($photo);
                //$res['photo'][$key]['sizes'] = $f->photos_getSizes($photo['id']);
            }
        }
        $this->params['res'] = $res;
        //

        return parent::render();
    }

    protected function parseSearchTagsConf()
    {
        $returnArr = array();
        $filePath = 'search_tags.conf';
        $conf = file_get_contents($filePath);
        $confArr = explode("\n", $conf);
        $cat = '';
        foreach($confArr as $row)
        {
            if (strpos($row, ':') !== false)
            {
                $cat = str_replace(':', '', trim($row));
            }
            $searchString = $cat;
            if (strpos($row, ':') === false)
            {
                $searchString .= ',' . trim($row);
            }
            
            $returnArr[str_replace(' ', '&nbsp;', $row)] = $searchString;
        }
        return $returnArr;
    }    
}
