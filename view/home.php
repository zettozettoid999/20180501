<div class="content">
    <div class="header">
        <form method="post">
            <input type="text" name="tags" id="tags" class="search_field"/>
            <select name="tags_dropdown" id="tags" class="search_field">
                <?foreach($this->searchTags as $tagName => $tagValue):?>
                <option value="<?=$tagValue?>"<? if (($this->tags) == $tagValue) { echo ' selected'; } ?>>
                    <?=$tagName?>
                </option>
                <?endforeach;?>
            </select>
            <input type="submit" name="search_button" id="search_button" value="Search Flickr" class="search_button" />
        </form>
    </div>
<?if (empty($this->res['photo'])):?>
    <div align="center">
        Unfortunately there were no photos in the response
        <br />
        for keywords <b><?=$this->tags?></b>
    </div>
<?else:?>
    <div class="header">
        Flickr Search Results
        <br />
        <br />
        for keywords <b><?=$this->tags?></b>

    </div>

    <table>
    <?foreach($this->res['photo'] as $photo):?>
        <tr class="photorow">
            <td class="photocol1">
                ID:
            </td>
            <td class="photocol2">
                <?=$photo['id']?>
            </td>
        </tr>
        <tr class="photorow">
            <td class="photocol1">
                Title:
            </td>
            <td class="photocol2">
                <?=$photo['title']?>
            </td>
        </tr>
        <tr>
            <td colspan="2" class="photocol3">
                <a href="<?=$photo['url']?>" target="_blank">
                <img src="<?=$photo['url']?>" border=0>
                </a>
            </td>
        </tr>

    <? endforeach; ?>
    </table>
<?endif;?>
</div>
